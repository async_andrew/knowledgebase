# 4. Построить грамматику, порождающую заданный язык.

## Более простые задачи даны в теме 2, особенно 20, 26

## Задача 4, вариант 45

Построить А-грамматику для языка $`L(G_1)L(G_2)`$, где

```math
G_1:
    \begin{cases}
        S \to acS | caC \\
        C \to cC | \lambda
    \end{cases}
```

```math
G_2:
    \begin{cases}
        S \to aS | cC \\
        C \to cC | \lambda
    \end{cases}
```

*Решение:*

Представим $`L(G_1)`$ и $`L(G_2)`$ в виде регулярного выражения:
```math
L(G_1) = (ac)^*cac^*
```
```math
L(G_2) = a^*cc^*
```
Отсюда
```math
L(G_1)L(G_2) = (ac)^*cac^*a^*cc^*
```

Сократить это выражение не получится, поэтому 
```mermaid
graph LR
    S((S))
    style S fill:#3f3,stroke:#000,stroke-width:4px
    A((A))
    style A fill:#fff,stroke:#000,stroke-width:4px
    B((B))
    style B fill:#fff,stroke:#000,stroke-width:4px
    C((C))
    style C fill:#fff,stroke:#000,stroke-width:4px
    D((D#))
    style D fill:#fff,stroke:#000,stroke-width:4px

    S--a-->A
    S--c-->B
    A--c-->S
    B--a-->C
    C--a-->C
    C--c-->C
    C--c-->D
    D--c-->D
```

По автомату строим грамматику:
```math
\begin{cases}
    S \to aA | cB \\
    A \to cA \\
    B \to aC \\
    C \to aC | cC | cD \\
    D \to cD | \lambda
\end{cases}
```