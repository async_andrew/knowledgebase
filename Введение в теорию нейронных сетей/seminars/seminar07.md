# Семинар 7й недели

## Многослойный перцептрон

Уравнение функционирования:
```math
\begin{cases}
h^l = \tilde{W}^l\tilde{y}^{l-1} \\
y^l = f_l(h^l)
\end{cases}
```

$`M`$ - количество входов

$`x=(x_1,...,x_M)^T`$
$`N_l`$ - число нейронов слоя $`l`$
$`K=N_L`$ - количество выходов
```math
\tilde{W}^l = 
\begin{pmatrix}
b_1^l & w_{11}^l &...&w_{1N_{L-1}} \\
... \\
b_{N_L}^l & w_{N_L1}^l &...&w_{N_LN_{L-1}}
\end{pmatrix}
```
$`y^0=x`$
$`\tilde{y}^l=(1,y_1^l,y_2^l,...,y_{N_L}^l)`$


### 2. Многослойный перцептрон как аппроксиматор.

В целом нейросети ориентированы на аппроксимацию, а не на интерполяцию.

<center><b>Задача 5-5.</b></center>

Рассмотрим $`i`$-й нейрон сети:
```math
\begin{cases}
h_i=w_{i0}^1x_0+w_{i1}^1x_1 \\
y_i=\exp{-\frac{h_i^2}{2}}
\end{cases}
```

$`x_0=1`$

```math
y_i=\exp
\left(
    -\frac{1}{2}
    (w_{i0}^1+w_{i1}^1x_1)^2
\right)=
```

Обычно представляем себе гауссиану как
```math
y=\exp(-\frac{1}{2}(\frac{x-m}{\sigma})^2)
```

```math
=
\exp
    \left(
        -\frac{1}{2}
        \left(
            \frac{
                x_1-
                \left(
                    -\frac{w_{i0}^1}{w_{i1}^1}
                \right)
            }{
                \frac{1}{w_{i1}^1}
            }
        \right)^2
    \right)
```

Можно сказать, что $`m=\left(-\frac{w_{i0}^1}{w_{i1}^1}\right)`$, а $`\sigma=\frac{1}{w_{i1}^1}`$

![рисунки гауссиан, которые друг с другом складываются]()

Отсюда видно, что можно этими гауссианами аппроксимировать любую функцию $`y=f(x)`$.

<center><b>Задача 5-6.</b></center>

Нейронная сеть имеет архитектуру, представленную на рисунке.

![](../images/seminar7_1.PNG)

Нейроны первого слоя имеют гауссову активационную характеристику, а выходной нейрон – логистическую.
Покажите, что настройкой синаптических коэффициентов и смещений нейронов сети возможно реализовать на ней колоколообразную функцию двух переменных.

Вместо $`s`$ будем писать $`y`$.
$`M=2`$.

![рисунок двумерной волны в трёхмерном пространстве]()

С помощью двух двумерный волн получили колокол, а с помощью логистической функции выходного нейрона сгладили неровности.

Задачу интерполяции нет смысла решать с помощью нейросети, т.к. предполагается, что входные данные имеют некоторую ошибку, поэтому нет нужды проходить ровно через точки.

### 3. Метод обратного распространения ошибки

Пусть есть обучающая выборка $`\{x^p,\sigma^p\}`$, $`p=\overline{1,P}`$.

Будем минимизировать:
```math
D
=
\frac{1}{2} \sum_{p=1}^P (\varepsilon^p)^2
= 
\frac{1}{2} \sum_{p=1}^P (\sigma^p-y^p)^2
=
\frac{1}{2} \sum_{p=1}^P \sum_{i=1}^{K=N_L} (\sigma_i^p-y_i^p)^2 \to \min_{\overline{W}}
```

```math
y^p=y^p(x^p,\overline{W})
```

```math
\overline{W} = vec(\tilde{W}^1,...,\tilde{W}^L)
```

```math
w_{ij}^l=w_{ij}^l(\tau)-
\alpha \frac{
    \partial D(\tau)
}{
    \partial w_{ij}^l
}
```

Ставится задача:
```math
\frac{
    \partial D(\tau)
}{
    \partial w_{ij}^l
} - ?
```

```math
\begin{matrix}
l=\overline{1,L} & i=\overline{1,N_l} & j=\overline{0,N_{l-1}}
\end{matrix}
```

Будем исходить из предположения, что $`P=1`$, т.е. после каждого примера будем считать ошибку. Это обучение по примерам.

```math
D=\frac{1}{2}\varepsilon^2
=
\frac{1}{2}\sum_{i=1}^K (\sigma_i-y_i)^2
=
\frac{1}{2}\sum_{i=1}^{N_L} (\sigma_i-y_i^L)^2
\to \min_{\overline{W}}
```

```math
\varDelta w_{ij}^l(\tau)
=
-\alpha\frac{
    \partial D(\tau)
}{
    \partial w_{ij}^l
}
```

```math
\frac{
    \partial D(\tau)
}{
    \partial w_{ij}^l
}
=
-\varDelta_i^l y_j^{l-1}
```

```math
\begin{matrix}
l=\overline{1,L} & i=\overline{1,N_l} & j=\overline{0,N_{l-1}}
\end{matrix}
```

$`\varDelta_i^l`$ - двойственные переменные.

```math
\varDelta_i^L = \varepsilon_i f_L'(h^L), \space i=\overline{1,N_L}
```
```math
\varDelta_i^l = \left( \sum_{m=1}^{N_{l+1}} \varDelta_m^{l+1}w_m^{l+1} \right)f_l'(h_i^l), \space i=\overline{1,N_l}, \space l=\overline{1,L-1}
```

```math
\sum_{m=1}^{N_{l+1}} \varDelta_m^{l+1}w_m^{l+1} = (\tilde{W}^{l+1})^T \varDelta^{l+1}
```

![рисунок с красивой сетью прямого распространения](../images/seminar7_2.PNG)

Мнемонические правила по построению сети обратного распространения:
1. Направление сигнала изменяется на противоположное.
2. Сумматорам в схеме прямого распространения соответствует разветвление сигнала в схеме обратного распространения; и наоборот.
3. Нелинейным элементам (активационным характеристикам) прямого соответствуют нелинейные элементы (блоки умножения)
4. К матрице синаптических коэффициентов применяется операция транспонирования.

<center><b>Задача 5-8.</b></center>

Для аппроксимации векторной функции одной переменной применяется нейронная сеть, содержащая два слоя.

![](../images/seminar7_3.PNG)

Первый слой содержит 2 нейрона с активационной характеристикой th(h), а второй – два нейрона с линейной активационной характеристикой. Обучение сети выполняется методом обратного распространения ошибки в режиме "по примерам".

Напишите выражения для производных критерия по всем настраиваемым синаптическим коэффициентам и постройте соответствующую схему обратного распространения ошибки.

```math
f_1(h)=\th(h)
```
```math
\th'(h)=1-(\th(h))^2
```
```math
f_1'(h)=1-y^2=1-(f_1(h))^2
```

```math
f_2(h)=h
```
```math
f_2'=1
```

```math
\varDelta_1^2 = \varepsilon_1f_2'(h_1^2)=\varepsilon_1
```
```math
\varDelta_2^2 = \varepsilon_2
```

```math
\varDelta_1^1
=
\left(
    \varDelta_1^2 w_{11}^2 + \varDelta_2^2 w_{21}^2
\right)
\left(
    1 - (y_1^1)^2
\right) 
= 
\left(
    \varepsilon_1 w_{11}^2 + \varepsilon_2 w_{21}^2
\right)
\left(
    1 - (y_1^1)^2
\right)
```

```math
\varDelta_2^1 
= 
\left(
    \varDelta_1^2 w_{12}^2 + \varDelta_2^2 w_{22}^2
\right)
\left(
    1 - (y_2^1)^2
\right) 
=
\left(
    \varepsilon_1 w_{12}^2 + \varepsilon_2 w_{22}^2
\right)
\left(
    1 - (y_2^1)^2
\right)
```

![](../images/seminar7_4.PNG)

```math
\begin{matrix}
\frac{
    \partial D
}{
    \partial w_{10}^2
}
=
\varDelta_1^2 y_0^1=\varepsilon_1

&

\frac{
    \partial D
}{
    \partial w_{20}^2
}
=
\varepsilon_2
\end{matrix}
```
```math
\begin{matrix}
\frac{
    \partial D
}{
    \partial w_{11}^2
}
=
-\varDelta_1^2y_1^1=-\varepsilon_1y_1^1

&

\frac{
    \partial D
}{
    \partial w_{21}^2
}
=
-\varepsilon_2y_1^1

\\

\frac{
    \partial D
}{
    \partial w_{12}^2
}
=
-\varDelta_1^2y_2^1=-\varepsilon_1y_2^1

&

\frac{
    \partial D
}{
    \partial w_{22}^2
}
=
-\varepsilon_2y_2^1

\end{matrix}
```

```math
\frac{
    \partial D
}{
    \partial w_{10}^1
}
=
-\varDelta_1^1y_0^0
=
-(\varDelta_1^2w_{11}^2+\varDelta_2^2w_{21}^2)(1-(y_1^1)^2)
=
-(\varepsilon_1w_{11}^2+\varepsilon_2w_{21}^2)(1-(y_1^1)^2)
```

```math
\frac{
    \partial D
}{
    \partial w_{11}^1
}
=
-\varDelta_1^1y_1^1
=
-(\varepsilon_1w_{11}^2+\varepsilon_2w_{21}^2)(1-(y_1^1)^2)x_1
```

<center><b>Задача 5-11.</b></center>

$`\th(h) \approx h`$, $`(h << 1)`$

```math
w_{11}^2(\tau+1)=w_{11}^2(\tau)-\alpha\frac{\partial D}{\partial w_{11}^2}
```
```math
w_{11}^2(\tau) = -0.2 \\
\alpha = 0.05
```

```math
\tilde{x}=\begin{pmatrix} 1 \\ 1 \end{pmatrix} = \tilde{y}^0
```
```math
\sigma=\begin{pmatrix} -0.1 \\ 0.3 \end{pmatrix}
```

```math
h^1=\tilde{W}^1 \tilde{y}^0=
\begin{pmatrix}
-0.5 & 0.4 \\
0.2 & -0.3
\end{pmatrix}
\begin{pmatrix}
1 \\ 1
\end{pmatrix}
=
\begin{pmatrix}
-0.1 \\ -0.1
\end{pmatrix}
```

```math
y^1 = f_1(h^1) \approx \begin{pmatrix} -0.1 \\ -0.1 \end{pmatrix}
```
```math
h^2 = \tilde{W}^2 \tilde{y}^1
=
\begin{pmatrix}
-0.1 & -0.2 & 0.01 \\
0.6 & 0.4 & -0.1
\end{pmatrix}
\begin{pmatrix}
1 \\ -0.1 \\ -0.1
\end{pmatrix}
=
\begin{pmatrix}
-0.001 \\ 0.57
\end{pmatrix}
```

```math
y^2=f_2(h^2)
=
\begin{pmatrix}
-0.081 \\ 0.515
\end{pmatrix}
```

```math
\varepsilon_1 = \sigma_1-y_1^2=-0.1+0.081=-0.019
```
```math
w_{11}^2(\tau+1)=-0.2-0.05\cdot0.019\cdot(-0.1)=-0.1999
```