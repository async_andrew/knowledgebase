# Лекция 3

### 4. Метод обратного распространения ошибки (backpropagation).

Задача обучения нейросети - это задача минимизации функции потерь.
```math
E(w)=\frac{1}{n}\sum_{i=1}^n L(F,(x,\sigma)) \to \min_w
```

Простой градиентный метод:
```math
w(\tau+1)=w(\tau)-\alpha \cdot \nabla E(w) \\
w(0)=w_0
```

![картинка с ямой]

Будем всё время идти в направлении наибольшего уменьшения градиента функции (антиградиента). 
* $`\alpha`$ - скорость обучения (параметр градиентного метода)
* $`\tau`$ - время обучения (номер итерации)
* $`\nabla E(w)`$ - градиент целевой функции, он неизвестен
```math
\nabla E(w) = \left(\frac{\partial E(w)}{\partial w_1},...,\frac{\partial E(w)}{\partial w_m}\right)^T
```

**Расчёт градиента для однослойной сети:**

![рисунок]()

Сеть из $`K`$ нейронов с $`M`$ входами.

Необходимо посчитать:
```math
\frac{\partial E(w)}{\partial w_{ij}} - ?
```

```math
E(w) = \frac{1}{n} \sum_{p=1}^n L(F,(x^{(p)},\sigma^{(p)})) = \frac{1}{n}\sum_{p=1}^nE^{p}(w)
```

```math
\frac{\partial E(w)}{\partial w_{ij}} = \frac{1}{n} \sum_{p=1}^n \frac{\partial E^{(p)}(w)}{\partial w_{ij}}
```

Т.е. $`E`$ от $`w`$ зависит не напрямую. Она зависит от $`y`$, а $`y`$ уже зависит от $`w`$.
```math
\frac{\partial E^{(p)}(w)}{\partial w_{ij}} = 
\sum_{k=1}^K 
    \frac{\partial E^{(p)}(w)}{\partial y_k^{(p)}} \cdot 
    \frac{\partial y_k^{(p)}(w)}{\partial w_{ij}} = 
\sum_{k=1}^K \frac{\partial E^{(p)}(w)}{\partial y_k^{(p)}} \cdot \frac{\partial y_k^{(p)}}{\partial h_k} \cdot \frac{\partial h_k(w)}{\partial w_{ij}} = 
```

Функция $`f_k`$ - это АХ $`k`$-го нейрона.
```math
= \sum_{k=1}^K \frac{\partial E^{(p)}(w)}{\partial y_k^{(p)}} \cdot f'_k(h_k^{(p)}) \cdot x_j^{(p)} \cdot \delta_{kj} =
```

```math
\frac{\partial k_l^{(p)}}{\partial w_{ij}} = 
\begin{cases}
0, & k\ne i\\
x_j^{(p)},& k = i
\end{cases} = x_j^{(p)} \cdot \delta_{kj}
```

```math
= 
\frac{
    \partial E^{(p)}(w)
}{
    \partial y_i^{(p)}
} 
\cdot f'_i(h_i^{(p)}) 
\cdot x_j^{(p)}, 
\space i=\overline{1,K}, 
\space j=\overline{1,M}
```

**Расчёт для двуслойной сети:**

![рисунок]()

Входов $`M`$, первый слой $`N`$, второй слой $`K`$, выходов столько же.

Практически то же самое, что и для однослойной сети, т.к. можно свести к взаимодействию двух слоёв.

```math
\frac{\partial E^{(p)}(w)}{\partial w_{ij}}=\frac{\partial E^{(p)}(w)}{\partial y_k^{(p)}} \cdot f'(h_k^{(p)}) \cdot y_i^{1,(p)}
```

```math
\frac{
    \partial E^{(p)}(w)
}{
    \partial v_{ij}
} = 
\sum_{k=1}^K
    \frac{
        \partial E^{(p)}(w)
    }{
        \partial y_k^{(p)}
    } 
    \cdot 
    \frac{
        \partial y_k^{(p)}(h_k^{(p)})
    }{
        \partial h_k^{(p)}
    }
    \cdot
    \frac{
        \partial h_k^{(p)}
    }{
        \partial v_{ij}
    }=
```
```math
= \sum_{k=1}^K
    \frac{
        \partial E^{(p)}(w)
    }{
        \partial y_k^{(p)}
    } 
    \cdot
    f'_2(h_k^{(p)})
    \cdot (
        w_{ki}f'_1(h_i^{(p)})x_j^{(p)}
    )

```

```math
h_k^{(p)} = 
    \sum_{l=0}^N 
        w_{kl} \cdot y_l^{1,(p)}(v_{ij})=
    \sum_{l=0}^N
        w_{kl} \cdot f_1(h_l^{(p)}(v_{ij}))
```
```math
\frac{
    \partial h_k^{(p)}
}{
    \partial v_{ij}
} = 
    \sum_{l=0}^N
        w_{kl} \frac{
            \partial f_1(h_l^{(p)})
        }{
            \partial h_l^{(p)}
        }
        \cdot
        \frac{
            \partial h_l^{(p)}
        }{
            \partial v_{ij}
        } = 
    \sum_{l=0}^N
        w_{kl} \cdot f'_1(h_l^{(p)}) \cdot x_j^{(p)} \cdot \delta_{il} =
    w_{ki}f'_1(h_i^{(p)})x_j^{(p)}
```

Так можно и для третьего слоя сделать, сделав последовательно вычисления для трёх слоёв.
> Протаскивая производную. (с) Трофимов

**Для многослойной сети.**

Уравнения обратного распространения ошибки:

```math
\frac{
    \partial E^{(p)}
}{
    \partial w_{ij}^l
} = 
    \Delta_i^{(p),l} \cdot y_i^{(p),l-1}
```
```math
\Delta^{(p),L}_i = 
    \frac{
        \partial E^{(p)}
    }{
        \partial y_i^{(p)}
    }
    \cdot f_L'(h_i^{(p),L})
```
```math
\Delta^{(p),l}_i = 
    \left(
        \sum_{j=1}^{N_{l+1}}
            \Delta_j^{(p),l+1} \cdot w_{ji}^{p+1}
    \right) 
    \cdot
    f_l'(h_i^{(p),l})
```
```math
i=\overline{1,N_l} \\
j=\overline{1,L-1}
```
> (с) 1986, Rumelhart

Можно составить конструкцию, которая состоит не из нейронов, а из блоков.

![рисунок с блоками]()

```math
\frac{
    \partial E^{(p)}
}{
    v_{ij}
} = ... \cdot
    \frac{
        \partial y_i
    }{
        \partial v_{ij}
    } \cdot ... =
    ... \cdot 
    \frac{
        \partial y_i
    }{
        \partial z_k
    } \cdot
    \frac{
        \partial z_k
    }{
        \partial v_{ij}
    } \cdot ...
```

**Алгоритм обратного распространения ошибки.**

1. Для примера $`x^{(p)}`$ из обучающей выборки рассчитать выход сети $`y^{(p)}`$. Т.н. прямой прогон.

2. Рассчитать потери $`E^{(p)}`$ на каждом примере $`(x^{(p)},\sigma^{(p)})`$.

3. Рассчитать $`\Delta_i^{(p),L}`$ для нейронов последнего слоя. Эти дельты называются двойственными потенциалами. $`\Delta_i^{(p),L} = \frac{\partial E^{(p)}}{\partial h_i^{(p),L}}`$.

4. Рассчитать двойственные потенциалы нейронов скрытых слоёв $`\Delta_i^{(p),l}`$.

5. Рассчитать производные $`E^{(p)}`$ по каждому синаптическому коэффициенту сети и смещению.

6. Повторить пункты 1-5 для всех примеров обучающей выборки.

**Рассчёт происзодных функций потерь:**
```math
\frac{
    \partial E^{(p)}
}{
    \partial y_k^{(p)}
} - ?
```

*1. Квадратичная.*

```math
E^{(p)}(w) = L(F,(x^{(p)},\sigma^{(p)})) = 
    \sum_{k=1}^K 
        (\sigma^{(p)}_k - y_k^{(p)})^2
```
```math
\frac{
    \partial E^{(p)}
}{
    \partial y_k^{(p)}
} = 
    -2 \left(
        \sigma_k^{(p)}-y_k^{(p)}
    \right) = 
    2e_k^{(p)}, \space k=\overline{1,K}
```

*2. Бинарная кросс-энтропия.*
K=1
```math
E^{(p)}(w) = L(F,(x^{(p)}),\sigma^{(p)})=
    -\left(
        \sigma^{(p)} \ln y^{(p)} 
        + 
        \left(
            1-\sigma^{(p)}
        \right) 
        \ln
        \left(
            1-y^{(p)}
        \right)
    \right)
```

```math
\frac{
    \partial E^{(p)}
}{
    \partial y_k^{(p)}
} = 
    -\left(
        \frac{\sigma^{(p)}}{y^{(p)}} - \frac{1-\sigma^{(p)}}{1-y^{(p)}}
    \right)=
    -\frac{
        \sigma^{(p)} - \sigma^{(p)} y^{(p)} -y^{(p)} + \sigma^{(p)} y^{(p)}
    }{
        y^{(p)}(1-y^{(p)})
    }=
```
```math
=
    \frac{
        y^{(p)}-\sigma^{(p)}
    }{
        y^{(p)}(1-y^{(p)})
    }=
    \frac{
        e^{(p)}
    }{
        y^{(p)}(1-y^{(p)})
    }
```

*3. Категориальная кросс-энтропия.*

```math
E^{(p)}(w) = L(F,(x^{(p)},\sigma^{(p)}))=
    -\sum_{k=1}^K
        \sigma^{(p)}_k \ln y^{(p)}_k
```

```math
\frac{
    \partial E^{(p)}
}{
    \partial y_k^{(p)}
} = 
    -\frac{
        \sigma_k^{(p)}
    }{
        y_k^{(p)}
    }, \space k=\overline{1,K}
```

Бинарная и категориальная кросс-энтропия интерпретируемых для своих задач классификации, в отличие от квадратичной. Это плюс.

**Пример.** Хотим обучить один одинокий нейрон. У него $`M`$ входов и логистическая функция активации. Возьмём обучающий пример $`(x^{(p)}, \sigma^{(p)})`$, $`\sigma^{(p)}=1`$. Мы задали $`w(0)=w_0`$, а на выходе вдруг получили $`y^{(p)} \approx 0`$.  
Что происходить будет? 

а) Зададим квадратичную функцию потерь:
```math
E^{(p)}(w)=(y^{(p)}-\sigma^{(p)})^2
```
```math
\frac{\partial E^{(p)}(w)}{\partial w_j}=
    2 \cdot 
    \left(
        y^{(p)}-\sigma^{(p)}
    \right) 
    \cdot 
    \frac{
        \partial y^{(p)}
    }{
        \partial w_j
    } = 
    2e^{(p)}f'(h^{(p)})x_j^{(p)} = 
```

```math
f(h) = \frac{1}{1+e^{-h}}
```
```math
f'(h) = f(h)(1-f(h))
```

```math
= 2e^{(p)}y^{(p)}(1-y^{(p)})x_j^{(p)}
```

б) Бинарная кросс-энтропия

```math
E^{(p)}(w) = 
-\left(
    \sigma^{(p)} \ln y^{(p)}
    +
    (1-\sigma^{(p)}) \ln (1-y^{(p)})
\right)
```

```math
\frac{\partial E^{(p)}(2)}{\partial w_j}
=
\frac{
    \partial e^{(p)}
}{
    \partial y^{(p)}(1-y^{(p)})
} \cdot
\frac{
    \partial y^{(p)}
}{
    \partial w_j
}
=
\frac{
    \partial e^{(p)}
}{
    \partial y^{(p)}(1-y^{(p)})
}
\cdot
y^{(p)}(1-y^{(p)})x_j^{(p)}
=
e^{(p)} x_j^{(p)}
```

Сравним:

|quade loss|cross-ent loss|
|:---:|:---:|
|![график 1]()|![график 2]()|

В скорости обучения и есть преимущество кросс-энтропии.